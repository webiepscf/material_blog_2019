<?php
/*
    ./app/controleurs/postsControleur.php
 */
namespace App\Controleurs\PostsControleur;
use \App\Modeles\PostsModele AS Post;

/**
 * [indexAction description]
 * @param  PDO    $connexion [description]
 * @return [type]            [description]
 */
function indexAction(\PDO $connexion) {
  include_once '../app/modeles/postsModele.php';
  $posts = Post\findAll($connexion);

  GLOBAL $content1, $title;
  $title = POSTS_INDEX_TITLE;
  ob_start();
    include '../app/vues/posts/index.php';
  $content1 = ob_get_clean();
}

function showAction(\PDO $connexion, int $id) {
  // Je vais chercher le post dans la DB
    include_once '../app/modeles/postsModele.php';
    $post = Post\findOneById($connexion, $id);

  // Je charge la vue
    GLOBAL $content1, $title;
    $title = $post['titre'];
    ob_start();
      include '../app/vues/posts/show.php';
    $content1 = ob_get_clean();
}
