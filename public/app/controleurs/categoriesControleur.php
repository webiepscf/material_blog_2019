<?php
/*
    ./app/controleurs/categoriesControleur.php
 */
namespace App\Controleurs\CategoriesControleur;
use \App\Modeles\CategoriesModele AS Categorie;

/**
 * [indexAction description]
 * @param  PDO    $connexion [description]
 * @return [type]            [description]
 */
function indexAction(\PDO $connexion) {
  include_once '../app/modeles/categoriesModele.php';
  $categories = Categorie\findAll($connexion);

  // Je charge directement la vue dans le template
  include '../app/vues/categories/index.php';
}
