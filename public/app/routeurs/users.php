<?php
/*
  ./app/routeurs/users.php
  Routes des users
  Il existe un $_GET['users']
 */


 switch ($_GET['users']) {
   /*
      FORMULAIRE DE CONNEXION
      PATTERN: index.php?users=loginForm
      CTRL: usersControleur
      ACTION: loginForm
    */
   case 'loginForm':
     include_once '../app/controleurs/usersControleur.php';
     \App\Controleurs\UsersControleur\loginFormAction();
     break;

   /*
      TRAITEMENT DU FORMULAIRE DE CONNEXION
      PATTERN: index.php?users=loginVerification
      CTRL: usersControleur
      ACTION: loginVerification
    */
   case 'loginVerification':
     include_once '../app/controleurs/usersControleur.php';
     \App\Controleurs\UsersControleur\loginVerificationAction($connexion, ['login' => $_POST['login'], 'pwd' => $_POST['pwd']]);
     break;

   default:
     // code...
     break;
 }
