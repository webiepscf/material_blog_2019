<?php
/*
    ./app/modeles/categoriesModele.php
 */
namespace App\Modeles\categoriesModele;

/**
 * [findAll description]
 * @param  PDO   $connexion [description]
 * @return array            [description]
 */
function findAll(\PDO $connexion) :array {
  $sql = "SELECT *
          FROM categories
          ORDER BY titre ASC;";

$rs = $connexion->query($sql);
return $rs->fetchAll(\PDO::FETCH_ASSOC);
}
