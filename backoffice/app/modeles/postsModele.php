<?php
/*
    ./app/modeles/postsModele.php
 */
namespace App\Modeles\PostsModele;

/**
 * [findAll description]
 * @param  PDO   $connexion [description]
 * @return array            [description]
 */
function findAll(\PDO $connexion) :array {
  $sql = "SELECT *, posts.id as postID
          FROM posts
          JOIN auteurs ON posts.auteur = auteurs.id
          ORDER BY posts.id DESC
          LIMIT 5;";

$rs = $connexion->query($sql);
return $rs->fetchAll(\PDO::FETCH_ASSOC);
}

/**
 * [findOneById description]
 * @param  PDO   $connexion [description]
 * @param  int   $id        [description]
 * @return array            [description]
 */
function findOneById(\PDO $connexion, int $id) :array {
  $sql='SELECT *, posts.id AS postID
        FROM posts
        JOIN auteurs ON posts.auteur = auteurs.id
        WHERE posts.id = :id;';
  $rs = $connexion->prepare($sql);
  $rs->bindValue(':id', $id, \PDO::PARAM_INT);
  $rs->execute();
  return $rs->fetch(\PDO::FETCH_ASSOC);
}

function insertOne(\PDO $connexion, array $data) :int {
  $sql = "INSERT INTO posts
          SET titre  = :titre,
              slug   = :slug,
              texte  = :texte,
              auteur = :auteur,
              datePublication = NOW();";
  $rs = $connexion->prepare($sql);
  $rs->bindvalue(':titre', $data['titre'], \PDO::PARAM_STR);
  $rs->bindvalue(':slug', $data['slug'], \PDO::PARAM_STR);
  $rs->bindvalue(':texte', $data['texte'], \PDO::PARAM_STR);
  $rs->bindvalue(':auteur', $data['auteur'], \PDO::PARAM_INT);
  $rs->execute();
  return $connexion->lastInsertId();
}

function insertCategorieById(\PDO $connexion, array $data) :bool {
  $sql = "INSERT INTO posts_has_categories
          SET post = :post,
              categorie = :categorie;";
  $rs = $connexion->prepare($sql);
  $rs->bindValue(':post', $data['postID'], \PDO::PARAM_INT);
  $rs->bindValue(':categorie', $data['categorieID'], \PDO::PARAM_INT);
  return $rs->execute();
}

function deletePostsHasCategoriesByPostId(\PDO $connexion, int $postID) :bool {
  $sql = "DELETE FROM posts_has_categories
          WHERE post = :post;";
  $rs = $connexion->prepare($sql);
  $rs->bindValue(':post', $postID, \PDO::PARAM_INT);
  return $rs->execute();
}

function deleteOneById(\PDO $connexion, int $id) :bool {
  $sql = "DELETE FROM posts
          WHERE id = :id;";
  $rs = $connexion->prepare($sql);
  $rs->bindValue(':id', $id, \PDO::PARAM_INT);
  return $rs->execute();
}

function findCategoriesByPostId(\PDO $connexion, int $postID) :array {
  $sql = "SELECT categorie FROM posts_has_categories
          WHERE post = :post;";
  $rs = $connexion->prepare($sql);
  $rs->bindValue(':post', $postID, \PDO::PARAM_INT);
  $rs->execute();
  return $rs->fetchAll(\PDO::FETCH_COLUMN);
}

function updateOneById(\PDO $connexion, int $id, array $data) :bool {
  $sql = "UPDATE posts
          SET titre  = :titre,
              slug   = :slug,
              texte  = :texte,
              auteur = :auteur
          WHERE id = :id;";
  $rs = $connexion->prepare($sql);
  $rs->bindValue(':id', $id, \PDO::PARAM_INT);
  $rs->bindValue(':titre', $data['titre'], \PDO::PARAM_STR);
  $rs->bindValue(':slug', $data['slug'], \PDO::PARAM_STR);
  $rs->bindValue(':texte', $data['texte'], \PDO::PARAM_STR);
  $rs->bindValue(':auteur', $data['auteur'], \PDO::PARAM_INT);
  return $rs->execute();
}
