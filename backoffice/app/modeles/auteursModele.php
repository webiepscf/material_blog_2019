<?php
/*
    ./app/modeles/auteursModele.php
 */
namespace App\Modeles\AuteursModele;

/**
 * [findAll description]
 * @param  PDO   $connexion [description]
 * @return array            [description]
 */
function findAll(\PDO $connexion) :array {
  $sql = "SELECT *
          FROM auteurs
          ORDER BY pseudo ASC;";

$rs = $connexion->query($sql);
return $rs->fetchAll(\PDO::FETCH_ASSOC);
}
