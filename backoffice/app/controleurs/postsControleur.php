<?php
/*
    ./app/controleurs/postsControleur.php
 */
namespace App\Controleurs\PostsControleur;
use \App\Modeles\PostsModele AS Post;

/**
 * [indexAction description]
 * @param  PDO    $connexion [description]
 * @return [type]            [description]
 */
function indexAction(\PDO $connexion) {
  include_once '../app/modeles/postsModele.php';
  $posts = Post\findAll($connexion);

  GLOBAL $content1, $title;
  $title = POSTS_INDEX_TITLE;
  ob_start();
    include '../app/vues/posts/index.php';
  $content1 = ob_get_clean();
}

function addFormAction(\PDO $connexion){
  // Je vais chercher les auteurs
  include_once '../app/modeles/auteursModele.php';
  $auteurs = \App\Modeles\AuteursModele\findAll($connexion);

  // Je vais chercher les catégories
  include_once '../app/modeles/categoriesModele.php';
  $categories = \App\Modeles\CategoriesModele\findAll($connexion);

  // Je charge la vue addForm dans $content1
  GLOBAL $content1, $title;
  $title = POSTS_ADDFORM_TITLE;
  ob_start();
    include '../app/vues/posts/addForm.php';
  $content1 = ob_get_clean();
}

function addInsertAction(\PDO $connexion) {
  // Je demande au modèle d'ajouter le post
    include_once '../app/modeles/postsModele.php';
    $id = Post\insertOne($connexion, $_POST);

  // Je demande au modèles d'ajouter les catégories correpondantes
    foreach ($_POST['categories'] as $categorieID) {
      $return = Post\insertCategorieById($connexion, [
        'postID' => $id,
        'categorieID' => $categorieID
      ]);
    }

  // Je redirige vers la liste des posts
    header('location: ' . ROOT_BACKOFFICE . 'posts');
}

function deleteAction(\PDO $connexion, int $id) {
  // Je demande au modèle de supprimer les liaisons N-M correspondantes
    include_once '../app/modeles/postsModele.php';
    $return1 = Post\deletePostsHasCategoriesByPostId($connexion, $id);

  // Je demande au modèle de supprimer le posts
    $return2 = Post\deleteOneById($connexion, $id);

  // Je redirige vers la liste des posts
    header('location: ' . ROOT_BACKOFFICE . 'posts');
}

function editFormAction(\PDO $connexion, int $id) {
  // Je demande au modèle le post à afficher dans le formulaire
    include_once '../app/modeles/postsModele.php';
    $post = Post\findOneById($connexion, $id);

  // Je demande au modèle les catégories du post
    include_once '../app/modeles/postsModele.php';
    $postCategories = Post\findCategoriesByPostId($connexion, $id);

  // Je vais chercher les auteurs
    include_once '../app/modeles/auteursModele.php';
    $auteurs = \App\Modeles\AuteursModele\findAll($connexion);

  // Je vais chercher les catégories
    include_once '../app/modeles/categoriesModele.php';
    $categories = \App\Modeles\CategoriesModele\findAll($connexion);

  // Je charge la vue editForm dans $content1
    GLOBAL $content1, $title;
    $title = POSTS_EDITFORM_TITLE;
    ob_start();
      include '../app/vues/posts/editForm.php';
    $content1 = ob_get_clean();
}

function editUpdateAction(\PDO $connexion, int $id) {
  // Je demande au modèle de supprimer toutes les catégories correpondantes
    include_once '../app/modeles/postsModele.php';
    $return1 = Post\deletePostsHasCategoriesByPostId($connexion, $id);

  // Je demande au modèle de modifier le post
    $return2 = Post\updateOneById($connexion, $id, $_POST);

  // Je demande au modèle d'ajouter les catégories correspondantes
    foreach ($_POST['categories'] as $categorieID) {
      $return = Post\insertCategorieById($connexion, [
        'postID' => $id,
        'categorieID' => $categorieID
      ]);
    }

  // Je redirige vers la liste des posts
    header('location: ' . ROOT_BACKOFFICE . 'posts');
}
