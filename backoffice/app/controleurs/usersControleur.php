<?php
/*
    ./app/controleurs/usersControleur.php
 */
namespace App\Controleurs\UsersControleur;
use \App\Modeles\UsersModele AS User;


function dashboardAction(\PDO $connexion) {
  GLOBAL $content1, $title;
  $title = USERS_DASHBOARD_TITLE;
  ob_start();
    include '../app/vues/users/dashboard.php';
  $content1 = ob_get_clean();
}

function logoutAction(){
  // Je tue la variable de session
    unset($_SESSION['user']);

  // Je redirige vers le site public
    header('location: ' . ROOT_PUBLIC);
}
