<?php
/*
  ./app/routeur.php
 */

 /*
   ROUTES DES POSTS
   PATTERN: index.php?posts=xxx&...
  */
 if(isset($_GET['posts'])):
   include_once '../app/routeurs/posts.php';

/*
  DECONNEXION DU USER
  PATTERN: index.php?users=logout
  CTRL: usersControleur
  ACTION: logout
 */
elseif(isset($_GET['users'])):
  include_once '../app/controleurs/usersControleur.php';
  \App\Controleurs\UsersControleur\logoutAction();

/*
  ROUTE PAR DEFAUT
  PATTERN: /
  CTRL: usersControleur
  ACTION: dashboard
 */
else:
  include_once '../app/controleurs/usersControleur.php';
  \App\Controleurs\UsersControleur\dashboardAction($connexion);
endif;
