<?php
/*
  ./app/routeurs/posts.php
  Routes des posts
  Il existe un $_GET['posts']
 */
use App\Controleurs\PostsControleur;
include_once '../app/controleurs/postsControleur.php';


 switch ($_GET['posts']) {
   /*
      FORMULAIRE d'AJOUT D'UN POST
      PATTERN: index.php?posts=addForm
      CTRL:  postsControleur
      ACTION: addForm
    */
   case 'addForm':
    PostsControleur\addFormAction($connexion);
    break;

    /*
       AJOUT D'UN POST: INSERT
       PATTERN: index.php?posts=addInsert
       CTRL:  postsControleur
       ACTION: addInsert
     */
    case 'addInsert':
     PostsControleur\addInsertAction($connexion);
     break;

    /*
      SUPPRESSION D'UN POST
      PATTERN: index.php?posts=delete&id=xxx
      CTRL:  postsControleur
      ACTION: delete
    */
    case 'delete':
      PostsControleur\deleteAction($connexion, $_GET['id']);
      break;

    /*
      EDITION D'UN POST: FORM
      PATTERN: index.php?posts=editForm&id=xxx
      CTRL:  postsControleur
      ACTION: editForm
    */
    case 'editForm':
      PostsControleur\editFormAction($connexion, $_GET['id']);
      break;

    /*
      EDITION D'UN POST: UPDATE
      PATTERN: index.php?posts=editUpdate&id=xxx
      CTRL:  postsControleur
      ACTION: editUpdate
    */
    case 'editUpdate':
      PostsControleur\editUpdateAction($connexion, $_GET['id']);
      break;

   /*
      LISTE DES POSTS
      PATTERN: index.php?posts=index
      CTRL:  postsControleur
      ACTION: index
    */
   default:
     PostsControleur\indexAction($connexion);
     break;
 }
