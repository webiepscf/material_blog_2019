<?php
/*
    ./app/vues/posts/index.php
    Variables disponibles:
    - $posts: ARRAY(ARRAY(id, titre, slug, datePublication, texte, media, auteur))
 */
 ?>
 <h1>Gestion des posts</h1>
 <div><a href="posts/add/form">Ajouter un enregistrement</a></div>

 <table class="table table-bordered">
   <thead>
     <tr>
       <th>Id</th>
       <th>Titre</th>
       <th>Slug</th>
       <th>datePublication</th>
       <th>Texte</th>
       <th>Media</th>
       <th>Auteur</th>
       <th></th>
     </tr>
   </thead>
   <tbody>
     <?php foreach ($posts as $post): ?>
       <tr>
          <td><?php echo $post['postID']; ?></td>
          <td><?php echo $post['titre']; ?></td>
          <td><?php echo $post['slug']; ?></td>
          <td><?php echo \Noyau\Fonctions\formater_date($post['datePublication'], 'd-m-Y'); ?></td>
          <td><?php echo \Noyau\Fonctions\tronquer($post['texte'], 20); ?></td>
          <td><img src="<?php echo $post['media']; ?>" alt="" width="50"></td>
          <td><?php echo $post['pseudo']; ?></td>
          <td>
            <a href="posts/<?php echo $post['postID']; ?>/edit/form">Edit</a> |
            <a href="posts/<?php echo $post['postID']; ?>/delete">Delete</a>
          </td>
        </tr>
     <?php endforeach; ?>
     </tbody>
 </table>
